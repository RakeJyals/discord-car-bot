import discord #for bot
import json #for filereading
import logging #for output of errors
import asyncio #for delays between trigger of discussion
import random #for choosing random url from list
from discord.ext import commands #for @botcommand
#for api
from clarifai.rest import ClarifaiApp
from clarifai.rest import Image as ClImage 

#Read json file
with open("../CarBotconfig") as config_file:
	config_json = json.load(config_file)
	token = config_json["token"]
	clariapi = config_json["clarifaiapi"]
	channelid = config_json["discussionchannel"]

#discord API variables, made bc they're classes (Jake ya dummy)
bot = commands.Bot(command_prefix="car_")

logging.basicConfig(level=logging.INFO)
urllist = [] #Used in add function

#Image detection API
app = ClarifaiApp(api_key=clariapi)
model = app.models.get("general-v1.3")

#Dev vars
slphrs = 6

async def periodic_trigger():
	while True:
		await asyncio.sleep(slphrs*60**2) #60^2sec is 1hr
		await discussion()

@bot.event
async def on_ready(): #set "playing...", currently broken
	print("ready")
#	await bot.change_status(game=discord.Game(name="car_hlp"))
	bot.loop.create_task(periodic_trigger()) #Starts loop of periodic_trigger

def is_car(img, message):
	imgstr = str(img) #ensures img is a string
	if not imgstr.startswith("http"): #check for image link
		if not len(message.attachments): #check for attached image
			return "fuck" #AKA invalid
		else:
			formaturl = str(message.attachments[0].url) #extract image url
	else:
		formaturl = imgstr #turns 2 variables into 1
	result = str(model.predict([ClImage(url=formaturl)])) #runs clarifai
	if "car" in result: #if car included as result
		return formaturl #passes url for caching
	else:
		return "!car"

async def discussion():
	discusschannel = bot.get_channel(channelid) #testing on uselessbots:general for now
	urllength = len(urllist)
	if urllength > 0:
		chosen = random.randint(0, urllength - 1)
		await discusschannel.send("Discuss: " + urllist[chosen]) #Broken, defined under dev vars
		urllist.pop(chosen)
	else:
		await discusschannel.send("There are no images to display. Next discussion in " + str(slphrs) + " hours")

@bot.command() #1 command per @
async def add(ctx, arg=None): #ctx-metadata, arg-message following command
	car = is_car(arg, ctx.message)
	if car.startswith("http"): #checks for postive, only time url is returned
		for existing in urllist: #checks to see if any urls are the same
			if car == existing:
				await ctx.send("We already have that one")
				return
		urllist.append(str(car)) #add url to variable list, make physical file in future?
		await ctx.send("Added")
	elif car.startswith("!car"): #drop chunk and just trigger test?
		await ctx.send("That's not a car!")
	else:
		await ctx.send("Action failed")

@bot.command()
async def hlp(ctx, arg=None): #Prints list of commands, remember to update, figure out how to override
	await ctx.send("Here's a list of my commands:\n***car_add-***Adds submissions to my cache of images, which will later be used in discussions \n***car_test-***Determines if the attached image contains a car\n")

@bot.command()
async def test(ctx, arg=None): #see add for understanding structure
	check = is_car(arg, ctx.message)
	if  check.startswith("http"):
		await ctx.send("Yep, that's a car")
	elif check.startswith("!car"):
		await ctx.send("That's not a car!")
	else:
		await ctx.send("Action failed")

bot.run(token)
